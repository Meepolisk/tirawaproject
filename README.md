# TirawaProject

A project for demonstrating Unity3D skill, to Tirawa Studio

=====================================================
Edit 10/10/2019
For TirawaStudio to test my skill, please get the project on master branch.
Total dev times this project is around 17.5 man hour with these features:

- Static top-down view on level
- Playground generator. currently has 4 procedural generated maps with difference tileset
- Couch co-op on same keyboard: 
   + Player 1: WASD for movement, J for bomb, K for denote special bomb
   + Player 2: ArrowKeys + Numpad 1, Numpad 2
- Player changeable stats: (but sadly, no pickup system implemented yet, change those hero attribute in inspector of Hero class)
   + fire blast lenght
   + current active bomb limitation
   + movement speed
   + remote-controlled bombs (actually not ingame yet but there is basecode for that already)
- Basic gameplay rule is the same with classic bomberman


**Future implementation: too many idea to write it here, better i just keep going ^^