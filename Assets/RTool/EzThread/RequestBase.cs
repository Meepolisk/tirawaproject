﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System.Threading;
using JetBrains.Annotations;

namespace RTool.EzThread
{
    public abstract class RequestBase
    {
        public const float DefaultTimeout = 5f;

        #region vars
        internal float initTimer = 0;

        private bool isFinished = false;
        public bool IsFinished => isFinished;

        private Exception exception = null;
        public Exception Exception => exception;
        internal void SetException(Exception ex)
        {
            if (isFinished)
                return;

            exception = ex;
        }
        private bool isCanceled = false;
        public bool IsCanceled => isCanceled;
        internal void SetCanceled(bool value)
        {
            if (isFinished)
                return;
            isCanceled = value;
        }

        public bool IsSucceeded => isFinished && (exception == null) && (isCanceled == false);

        protected CancellationTokenSource timeoutCancellationTokenSource { get; set; }
        #endregion
        internal RequestBase() { }
        internal RequestBase(float initTimer) => this.initTimer = initTimer;

        protected virtual void CallBack_Finished()
            => onFinished.ForEach(func => func(this));

        public bool IsStarted { get; private set; }
        protected virtual void Start()
        {
            IsStarted = true;
            Debug.Log("RequestBase started");
            if (initTimer > 0)
                StartTimer(initTimer);
        }
        internal void InternalStart() => Start();
        private Task timerTask { get; set; }
        internal void StartTimer(float timeOut)
        {
            if (isFinished == true) return;
            if (timeOut <= 0) throw new Exception("TimeOut must greater than Zero");

            CancelTimer();
            timeoutCancellationTokenSource = new CancellationTokenSource();
            
            TaskScheduler taskScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            using (timeoutCancellationTokenSource)
            {
                timerTask = Task.Delay(TimeSpan.FromSeconds(timeOut)).ContinueWithUnityThread(task =>
                {
                    if (task.IsCompleted && IsFinished == false)
                    {
                        exception = new TimeoutException("Request timed out");
                        OnRequestCompleteInternalCall();
                    }
                }, timeoutCancellationTokenSource.Token);
            }
        }
        public void ForceFail(Exception exception)
        {
            SetException(exception);
            OnRequestComplete();
        }
        public void CancelTimer() => timeoutCancellationTokenSource?.Cancel();
        internal List<Action<object>> onFinished = new List<Action<object>>();

        protected void OnRequestComplete()
        {
            if (isFinished == true)
                return;
            timerTask = null;
            timeoutCancellationTokenSource = null;

            isFinished = true;
            CallBack_Finished();
        }
        internal void OnRequestCompleteInternalCall() => OnRequestComplete();

    }

    public static partial class RequestExtensionMethods
    {
        public static T onFinished<T>(this T request, bool willStart, [NotNull] Action<T> onFinishedAction) where T : RequestBase
        {
            //request.onFinished.Add(onFinishedAction as Action<dynamic>);
            request.onFinished.Add(new Action<object>(o => onFinishedAction((T)o)));
            if (request.IsFinished)
                onFinishedAction.Invoke(request);
            if (willStart)
                request.Run();
            return request;
        }
        public static T onFinished<T>(this T request, Action<T> onFinishedAction) where T : RequestBase
            => request.onFinished(true, onFinishedAction);
        public static T TimeOut<T>(this T request, float seconds = RequestBase.DefaultTimeout) where T : RequestBase
        {
            request.StartTimer(seconds);
            return request;
        }
        public static T Run<T> (this T request) where T : RequestBase
        {
            request.InternalStart();
            return request;
        }
    }
}
