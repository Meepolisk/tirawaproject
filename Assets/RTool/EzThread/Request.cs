﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System.Threading;

namespace RTool.EzThread
{
    public sealed class Request : RequestBase
    {
        private Task task;

        /// <summary>
        /// Start empty basic Request. Shouldnt call this
        /// </summary>
        public Request() { }
        /// <summary>
        /// Start empty basic Request with init timer.
        /// </summary>
        /// <param name="timeOut">In seconds</param>
        public Request(float timeOut) : base(timeOut) { }
        public Request(Task task) => this.task = task;
        public Request(Action action) => task = new Task(action);

        protected override void Start()
        {
            if (IsStarted == true)
                return;

            base.Start();
            if (task == null)
                return;
            
            task.ContinueWithUnityThread(internalTask =>
            {
                if (IsFinished == true)
                    return;

                if (internalTask.IsFaulted)
                    SetException(internalTask.Exception);
                else if (internalTask.IsCanceled)
                    SetException(new TaskCanceledException("Task cancelled by user"));
                else if (!internalTask.IsCompleted)
                    SetException(new Exception("Unknown Exception"));
                OnRequestCompleteInternalCall();
            });
        }

        public static Request Create(Task task) => new Request(task);
        public static Request Create(Action action) => new Request(action);

        public void ForceSucceed() => OnRequestComplete();

        /// <summary>
        /// Static function, return a failed Request with Exception
        /// </summary>
        /// <returns></returns>
        public static Request Fail(Exception exception)
        {
            Request request = new Request();
            request.SetException(exception);
            request.OnRequestComplete();
            return request;
        }
        /// <summary>
        /// Static funtion, return a succeeded Request
        /// </summary>
        /// <returns></returns>
        public static Request Succeed()
        {
            Request request = new Request();
            request.ForceSucceed();
            return request;
        }

    }
    public sealed class Request<TResult> : RequestBase
    {
        private Task<TResult> task;
        private TResult result;
        public TResult Result => result;

        /// <summary>
        /// Start empty basic Request. Shouldnt call this
        /// </summary>
        public Request() : base() { }
        /// <summary>
        /// Start empty basic Request with init timer.
        /// </summary>
        /// <param name="timeOut">In seconds</param>
        public Request(float timeOut) : base(timeOut) { }
        public Request(Task<TResult> task) => this.task = task;
        public Request(Func<TResult> func) => task = new Task<TResult>(func);

        protected override void Start()
        {
            if (IsStarted)
                return;

            base.Start();
            if (task == null)
                return;

            TaskScheduler taskScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            task.ContinueWithUnityThread(internalTask =>
            {
                if (IsFinished == true)
                    return;

                if (internalTask.IsCompleted)
                    result = task.Result;
                if (internalTask.IsFaulted)
                    SetException(internalTask.Exception);
                else if (internalTask.IsCanceled)
                    SetException(new TaskCanceledException("Task cancelled by user"));
                else
                    SetException(new Exception("Unknown Exception"));
                OnRequestCompleteInternalCall();
            });
        }

        public void ForceSucceed(TResult result)
        {
            this.result = result;
            OnRequestComplete();
        }

        public static Request<TResult> Create (Task<TResult> task) => new Request<TResult>(task);
        public static Request<TResult> Create (Func<TResult> func) => new Request<TResult>(func);
        /// <summary>
        /// Static function, return a failed Request with Exception
        /// </summary>
        /// <returns></returns>
        public static Request<TResult> Fail(Exception exception)
        {
            Request<TResult> request = new Request<TResult>();
            request.SetException(exception);
            request.OnRequestComplete();
            return request;
        }
        /// <summary>
        /// Static function, return a succeed Request with value;
        /// </summary>
        /// <returns></returns>
        public static Request<TResult> Succeed(TResult result)
        {
            Request<TResult> request = new Request<TResult>();
            request.result = result;
            request.OnRequestComplete();
            return request;
        }
    }
    public static class RequestExtension
    {
        public static Request Request(this Task task) => new Request(task);
        public static Request Request(this Action action) => EzThread.Request.Create(action);
        
        public static Request<T> Request<T>(this Task<T> task) => new Request<T>(task);
        public static Request<T> Request<T>(this Func<T> func) => new Request<T>(func);
    }

    public static class ThreadExtension
    {
        public static Task ContinueWithUnityThread(this Task task, Action<Task> continueAction)
            => task.ContinueWith(continueAction, TaskScheduler.FromCurrentSynchronizationContext());
        public static Task ContinueWithUnityThread(this Task task, Action<Task> continueAction, CancellationToken cancellationToken)
            => task.ContinueWith(continueAction, cancellationToken, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());

        public static Task<T> ContinueWithUnityThread<T>(this Task<T> task, Action<Task<T>> continueAction)
            => task.ContinueWith(continueAction, TaskScheduler.FromCurrentSynchronizationContext()) as Task<T>;
        public static Task<T> ContinueWithUnityThread<T>(this Task<T> task, Action<Task<T>> continueAction, CancellationToken cancellationToken)
            => task.ContinueWith(continueAction, cancellationToken, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext()) as Task<T>;

        public static T TimeOutAfter<T>(this T task, float timeOut) where T : Task
        {
            using (var timeoutCancellationTokenSource = new CancellationTokenSource())
            {
                Task.WhenAny(task, Task.Delay(TimeSpan.FromSeconds(timeOut), timeoutCancellationTokenSource.Token)).ContinueWith(internalTask =>
                {
                    if (internalTask.Result == task)
                        timeoutCancellationTokenSource.Cancel();
                    else
                        throw new TimeoutException("The operation has timed out.");
                });
            }
            return task;
        }
    }
}
