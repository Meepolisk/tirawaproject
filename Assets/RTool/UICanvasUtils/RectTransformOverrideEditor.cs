﻿//#if UNITY_EDITOR
//using UnityEngine;
//using UnityEditor;
//using System.Reflection;
//using System;

//namespace RTool.UICanvasUtil
//{
//    //ref link: https://github.com/Unity-Technologies/UnityCsReference/blob/master/Editor/Mono/Inspector/RectTransformEditor.cs

//    [CustomEditor(typeof(RectTransform), true)]
//    public class RectTransformEditor : Editor
//    {
//        private Editor editorInstance;

//        private void OnEnable()
//        {
//            Assembly ass = Assembly.GetAssembly(typeof(Editor));
//            Type rtEditor = ass.GetType("UnityEditor.RectTransformEditor");
//            editorInstance = CreateEditor(target, rtEditor);
//        }

//        public override void OnInspectorGUI()
//        {
//            editorInstance.OnInspectorGUI();
//            EditorGUILayout.HelpBox("RTool is driven some attribute", MessageType.Info);
//        }

//        private void OnSceneGUI()
//        {
//            MethodInfo onSceneGUI_Method = editorInstance.GetType().GetMethod("OnSceneGUI", BindingFlags.NonPublic | BindingFlags.Instance);
//            onSceneGUI_Method.Invoke(editorInstance, null);
//        }
//    }
//}
//#endif