﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

namespace RTool.Utils
{
    public static class ExtensionMethods
    {
        public static void Push<T>(this List<T> _list, T _object)
        {
            _list.Add(_object);
        }
        public static T PeekLast<T>(this List<T> _list) where T : class
        {
            if (_list.Count == 0)
                return null;

            return _list[_list.Count - 1];
        }
        public static T PeekFirst<T>(this List<T> _list) where T : class
        {
            if (_list.Count == 0)
                return null;

            return _list[0];
        }
        public static T Pop<T>(this List<T> _list) where T : class
        {
            if (_list.Count == 0)
                return null;
            var index = _list.Count - 1;
            var removedOne = _list[index];
            _list.RemoveAt(index);

            return removedOne;
        }

        public static T GetRandom<T>(this List<T> _list)
            => _list[UnityEngine.Random.Range(0, _list.Count)];

        public static void ChangeX(this Vector3 vector, float value) => vector.x = value;
        public static void ChangeY(this Vector3 vector, float value) => vector.y = value;
        public static void ChangeZ(this Vector3 vector, float value) => vector.z = value;

        public static void ChangeX(this Vector2 vector, float value) => vector.x = value;
        public static void ChangeY(this Vector2 vector, float value) => vector.y = value;
    }
}