﻿using System;
using System.Collections.Generic;
using UnityEngine;
using RTool.Utils;

namespace RTool.EzCanvas
{
    public partial class MonoCanvas : BaseCanvas
    {
        #region static MonoController
        private static Dictionary<string, List<MonoCanvas>> monoStackDict = new Dictionary<string, List<MonoCanvas>>();
        
        public static void ReturnToPreviousMenu(string menuGroup = "", bool ignoreTransition = false)
        {
            if (string.IsNullOrEmpty(menuGroup))
                menuGroup = string.Empty;

            monoStackDict[menuGroup].PeekLast().Hide(ignoreTransition);
        }

        public static void PagingNewMono(MonoCanvas mono, bool ignoreTransition)
        {
            string groupName = mono.monoGroup;
            if (!monoStackDict.ContainsKey(groupName))
                monoStackDict.Add(groupName, new List<MonoCanvas>());

            List<MonoCanvas> stack = monoStackDict[groupName];
            if (stack.Count > 0)
            {
                if (stack.PeekLast() == mono)
                    return;

                MonoCanvas current = stack.PeekLast();
                current.SetEnable(false);
                current.TransferCanvas(mono).onComplete((oldCanvas, newCanvas) =>
                {
                    stack.Push(newCanvas as MonoCanvas);
                });
            }
            stack.Push(mono);
        }
        public static void PoppinOldMono(MonoCanvas mono, bool ignoreTransition)
        {
            string groupName = mono.monoGroup;
            if (!monoStackDict.ContainsKey(groupName))
                return;

            List<MonoCanvas> stack = monoStackDict[groupName];
            if (stack.Count > 0)
            {
                MonoCanvas current = stack.PeekLast();
                current.SetEnable(false);
                if (stack.Count > 1)
                    current.TransferCanvas(null).onComplete((oldCanvas, newCanvas) =>
                    {
                        stack.Pop();
                    });
            }
        }
        public static void UnRegistMono (MonoCanvas mono)
        {
            string groupName = mono.monoGroup;
            if (!monoStackDict.ContainsKey(groupName))
                return;

            List<MonoCanvas> stack = monoStackDict[groupName];
            if (stack.Contains(mono))
            {
                MonoCanvas replacement = null;
                if (stack.Count > 1 && stack.PeekLast() == mono)
                    replacement = stack[stack.Count - 2];
                mono?.Hide(true);
                stack.Remove(mono);
                replacement?.Show(true);
            }
        }
        #endregion
        [SerializeField]
        private string monoGroup = "";
        public string MonoGroup => monoGroup;

        protected virtual void OnDestroy()
        {
            UnRegistMono(this);
        }

        #region public call
        public override void Show (bool ignoreTransition = false) => PagingNewMono(this, ignoreTransition);
        public override void Hide (bool ignoreTransition = false) => PoppinOldMono(this, ignoreTransition);
        #endregion
    }
}
