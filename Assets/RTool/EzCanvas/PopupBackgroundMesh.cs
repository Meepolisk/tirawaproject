﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RTool;
using RTool.Utils;

namespace RTool.EzCanvas
{
    [RequireComponent(typeof(Button), typeof(Image), typeof(RectTransform))]
    public class PopupBackgroundMesh : MonoBehaviour
    {
        private readonly Color meshColor = new Color(0, 0, 0, 0);

        private Button fakeButton { get; set; }
        private Image meshImage { get; set; }
        private RectTransform rectTransform { get; set; }
        private PopupBackground handler { get; set; }

        private float FadeInDuration => handler.FadeInDuration;
        private float FadeOutDuration => handler.FadeOutDuration;
        private float FullMaskAlpha => handler.FullMaskAlpha;
        private float imageAlpha
        {
            get => meshImage.color.a;
            set
            {
                var color = meshImage.color;
                color.a = Mathf.Clamp01(value);
                meshImage.color = color;
            }
        }
        public float AlphaValue
        {
            get => Mathf.Clamp01(imageAlpha / FullMaskAlpha);
            private set => imageAlpha = value * FullMaskAlpha;
        }
        public bool Enable
        {
            get => meshImage.raycastTarget;
            set => meshImage.raycastTarget = value;
        }
        //eventually, Awake will happen before Setup called from Handler
        private void Awake()
        {
            meshImage = GetComponent<Image>();
            meshImage.raycastTarget = false;
            rectTransform = GetComponent<RectTransform>();
            rectTransform.sizeDelta = Vector2.zero;
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.one;
            rectTransform.anchoredPosition = Vector2.zero;

            fakeButton = GetComponent<Button>();
            fakeButton.transition = Selectable.Transition.None;
            fakeButton.onClick.AddListener(() =>
            {
                handler.BackgroundMeshClicked();
            });
        }
        protected void OnDestroy()
        {
            StopAllCoroutines();
        }
        internal void Setup(PopupBackground _handler)
        {
            handler = _handler;
            meshImage.color = new Color(meshColor.r, meshColor.g, meshColor.b, 0f);

            Enable = false;
        }

        public void Show(bool _isOn = true)
        {
            if (_isOn == true)
            {
                Enable = true;
                StartCoroutine(_FadeIn(FadeInDuration));
            }
            else
            {
                StartCoroutine(_FadeOut(FadeOutDuration));
            }
        }

        private IEnumerator _FadeIn(float maxDur)
        {
            float _timeTick = AlphaValue * maxDur;
            while (true)
            {
                //Debug.Log(_timeTick);
                yield return null;
                _timeTick += Time.deltaTime;
                if (_timeTick >= maxDur)
                    break;
                AlphaValue = (_timeTick / maxDur);
            }
            AlphaValue = 1;
        }

        private IEnumerator _FadeOut(float maxDur)
        {
            float _timeTick = AlphaValue * maxDur;
            while (true)
            {
                //Debug.Log(_timeTick);
                yield return null;
                _timeTick -= Time.deltaTime;
                if (_timeTick <= 0)
                    break;
                AlphaValue = (_timeTick / maxDur);
            }
            AlphaValue = 0;
            Enable = false;
        }
        
    }
}
