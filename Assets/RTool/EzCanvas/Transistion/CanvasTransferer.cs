﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RTool.EzCanvas
{
    public partial class BaseCanvas : MonoBehaviour //CanvasTransferer
    {
        public sealed class CanvasTransferer
        {
            private BaseCanvas closeCanvas { get; set; }
            private BaseCanvas openCanvas { get; set; }
            private bool ignoreTransition { get; set; }

            public CanvasTransferer([NotNull] BaseCanvas closeCanvas, BaseCanvas openCanvas = null, bool ignoreTransition = false)
            {
                if (closeCanvas.Enable == true)
                    closeCanvas.Hide(ignoreTransition);
                this.closeCanvas = closeCanvas;
                this.openCanvas = openCanvas;
                this.ignoreTransition = ignoreTransition;

                this.closeCanvas.onHideFinished += WaitCanvas_onHideFinished;
                this.closeCanvas.onShowFinished += WaitCanvas_onShowFinished;
            }
            public delegate void TransferDelegate(BaseCanvas waitCanvas, BaseCanvas nextCanvas);
            private TransferDelegate onCompleteDelegate;
            public CanvasTransferer onComplete(TransferDelegate onCompleteDelegate)
            {
                this.onCompleteDelegate = onCompleteDelegate;
                return this;
            }

            private void WaitCanvas_onShowFinished(BaseCanvas baseCanvas)
            {
                onCompleteDelegate?.Invoke(closeCanvas, openCanvas);
                if (openCanvas != null)
                    openCanvas.SetEnable(true, ignoreTransition);
                UnregEvent();
            }

            private void WaitCanvas_onHideFinished(BaseCanvas baseCanvas)
            {
                UnregEvent();
            }

            private void UnregEvent()
            {
                onCompleteDelegate = null;
                closeCanvas.onHideFinished -= WaitCanvas_onHideFinished;
                closeCanvas.onShowFinished -= WaitCanvas_onShowFinished;
            }
        }
    }
}
