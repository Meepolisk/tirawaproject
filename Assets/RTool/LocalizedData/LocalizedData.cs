﻿using UnityEngine;
using RTool.Localization;

[System.Serializable]
public class LocalizedString : LocalizedDataBase<string> { };

[System.Serializable]
public class LocalizedTexture : LocalizedDataBase<Texture> { };