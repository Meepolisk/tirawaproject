﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace RTool.Localization
{
    [System.Serializable]
    public abstract partial class LocalizedDataBase
    {
        [SerializeField]
        protected LocalizedDataManager source;
        
        [SerializeField]
        protected string idKey;
    }
    [System.Serializable]
    public abstract partial class LocalizedDataBase<T> : LocalizedDataBase
    {
        public T Value => source.GetData<T>(idKey);
    }
}