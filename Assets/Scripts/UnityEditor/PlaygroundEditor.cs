﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using REditor;
using RTool;
using RTool.Utils;

public partial class Playground : MonoBehaviour //editor
{
    [Header("Component Ref: objects")]
    [SerializeField]
    private Transform plane = null;
    [SerializeField]
    private Transform leftBorder = null;
    [SerializeField]
    private Transform rightBorder = null;
    [SerializeField]
    private Transform topBorder = null;
    [SerializeField]
    private Transform bottomBorder = null;

    [Header("Component Ref: prefabs")]
    [SerializeField]
    private StaticBlock staticBlockPrefab = null;
    [SerializeField]
    private List<Block> blockPrefabs = null;

    [SerializeField, HideInInspector]
    private List<StaticBlock> staticBlocks = new List<StaticBlock>();

    [SerializeField, HideInInspector]
    private List<Vector2Int> blockPositions = new List<Vector2Int>();

    private void ChangeMapSize()
    {
        float width = sizeX * 2 + 1;
        float height = sizeY * 2 + 1;
        Debug.LogFormat("Generating Map with block size({0}, {1})", height, width);
        plane.localPosition = Vector3.zero;
        plane.localRotation = Quaternion.identity;
        plane.localScale = new Vector3(width * 0.2f, 1f, height * 0.2f);

        topBorder.localPosition = new Vector3(-width - 0.5f, 1f, 0f);
        topBorder.localRotation = Quaternion.identity;
        topBorder.localScale = new Vector3(1f, 2f, height*2f + 2f);

        bottomBorder.localPosition = new Vector3(width + 0.5f, 1f, 0f);
        bottomBorder.localRotation = Quaternion.identity;
        bottomBorder.localScale = new Vector3(1f, 2f, height * 2f + 2f);

        leftBorder.localPosition = new Vector3(0f, 1f, -height - 0.5f);
        leftBorder.localRotation = Quaternion.identity;
        leftBorder.localScale = new Vector3(width * 2f + 2f, 2f, 1f);

        rightBorder.localPosition = new Vector3(0f, 1f, height + 0.5f);
        rightBorder.localRotation = Quaternion.identity;
        rightBorder.localScale = new Vector3(width * 2f + 2f, 2f, 1f);

        //clear old static blocks
        if (staticBlocks.Count > 0)
        {
            staticBlocks.ForEach(block => DestroyImmediate(block.gameObject));
            staticBlocks.Clear();
        }

        //generating static blocks
        int blockCount = (int)sizeY * (int)sizeX;
        for (int y = 1; y < height; y+=2)
        {
            for(int x = 1; x < width; x+=2)
            {
                StaticBlock newBlock = PrefabUtility.InstantiatePrefab(staticBlockPrefab, transform) as StaticBlock;
                newBlock.EditorSetup(this, x, y, "StaticBlock");
                staticBlocks.Add(newBlock);
            }
        }
        
        //clear old blocks
        if (blocks.Count > 0)
        {
            blocks.ForEach(block => DestroyImmediate(block.gameObject));
            blocks.Clear();
        }

        //gereration destroyable blocks
        foreach (var pos in blockPositions)
        {
            Block newBlock = PrefabUtility.InstantiatePrefab(blockPrefabs.GetRandom(), transform) as Block;
            newBlock.EditorSetup(this, pos.x, pos.y, "Block");
            blocks.Add(newBlock);
        }
    }

    [CustomEditor(typeof(Playground))]
    private class Inspector : UnityObjectEditor<Playground>
    {
        protected override void DrawingInspector()
        {
            if (Application.isPlaying == true)
                return;

            folded = GUILayout.Toggle(folded, new GUIContent("Block Position"), EditorStyles.foldout);
            if (folded == false)
                DrawBlockController();
            DrawGenerateButton();
        }

        private const float btnSize = 25f;
        private bool folded = true;
        private void DrawBlockController()
        {
            Color trueColor = GUI.backgroundColor;
            GUILayout.BeginVertical();
            int height = (int)handler.BlockHeight;
            int width = (int)handler.BlockWidth;
            for (int y = height - 1; y >= 0; y--)
            {
                GUILayout.BeginHorizontal();
                for (int x = 0; x < width; x++)
                {
                    Vector2Int checkingPos = new Vector2Int(x, y);
                    //make all the static block position red and uncheckable
                    if (checkingPos.IsStaticBlockPosition(handler))
                    {
                        GUI.enabled = false;
                        GUI.backgroundColor = Color.red;
                        GUILayout.Toggle(false, new GUIContent("", string.Format("[{0}:{1}] is static block", x, y)), EditorStyles.toolbarButton);
                        GUI.enabled = true;
                    }
                    //make spawn position gray and uncheckable
                    else if (checkingPos.IsSpawnPosition(handler))
                    {
                        GUI.enabled = false;
                        GUI.backgroundColor = Color.gray;
                        GUILayout.Toggle(false, new GUIContent("", string.Format("[{0}:{1}] is spawn position", x, y)), EditorStyles.toolbarButton);
                        GUI.enabled = true;
                    }
                    else
                    {
                        bool currentPosHasBlock = handler.blockPositions.Contains(checkingPos);
                        GUI.backgroundColor = currentPosHasBlock? Color.yellow : trueColor;
                        bool newValue = GUILayout.Toggle(currentPosHasBlock, new GUIContent("", string.Format("[{0}:{1}]", x, y)), EditorStyles.toolbarButton);
                        if (newValue != currentPosHasBlock)
                        {
                            if (newValue == true)
                            {
                                handler.blockPositions.Add(checkingPos);
                            }
                            else
                            {
                                handler.blockPositions.Remove(checkingPos);
                            }
                        }
                    }
                    GUI.backgroundColor = trueColor;
                }
                GUILayout.EndHorizontal();
            }
            //Add button to "clear" all destructable block. Better have this feature
            if (GUILayout.Button(new GUIContent("Clear Block")))
            {
                if (EditorUtility.DisplayDialog("Meow, Are you surr??", "Meow meow meow meow meow?", "OK", "Cancel"))
                {
                    handler.blockPositions.Clear();
                }
            }
            GUILayout.EndVertical();
        }

        private void DrawGenerateButton()
        {
            if (GUILayout.Button("Generate"))
            {
                handler.ChangeMapSize();
                folded = true;
            }
        }
    }
}
#endif