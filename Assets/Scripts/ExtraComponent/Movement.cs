﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTool;

public class Movement : MonoBehaviour
{
    [Header("Component Ref")]
    [SerializeField]
    private Rigidbody rigid = null;

    [Header("Config")]
    [SerializeField, Range(5,20)]
    private float movementSpeed = 7f;
    public float MovementSpeed => movementSpeed;

    private short disableTick = 0;
    public bool IsEnabled
    {
        get => disableTick <= 0;
        set
        {
            if (value == true)
                disableTick--;
            else
                disableTick++;
        }
    }
    public float CurrentMovementVelocity { get; set; }

    [SerializeField]
    private Vector2 input;
    public Vector2 Input
    {
        get => IsEnabled ? input : Vector2.zero;
        set => input = value;
    }

    private void FixedUpdate()
    {
        //prevent floating on y-axies
        Vector3 newVelo = rigid.velocity;
        if (Input != Vector2.zero)
        {
            newVelo.x = Input.x * movementSpeed;
            newVelo.z = Input.y * movementSpeed;
            
            ActionFaceDirection(Input);
        }
        else
        {
            newVelo.x = 0f;
            newVelo.z = 0f;
        }
        CurrentMovementVelocity = newVelo.magnitude;
        rigid.velocity = newVelo;
    }

    public void ActionFaceDirection(Vector2 _direction)
    {
        float z = _direction.DirectionToYRotation();
        Vector3 euler = transform.eulerAngles;
        euler.y = z;

        transform.eulerAngles = euler;
    }
}

public static partial class ExtensionMethods
{
    public static float DirectionToYRotation(this Vector2 _vec)
    {
        Vector2 compareVec = Vector2.up;
        float result = Vector2.Angle(_vec, compareVec);
        if (_vec.x < 0)
            result *= -1f;
        return result;
    }
}