﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTool;

public class Hero : Unit
{
    #region attribute / field
    private const int maxBombAllow = 8;
    private const int maxBlastAllow = 15;

    [SerializeField]
    private RInputProfile inputProfile = null;

    [SerializeField]
    private TimedBomb normalBombPrefabs = null;

    [SerializeField, Range(1, maxBombAllow)]
    private int bombLevel = 1;
    public int BombLevel
    {
        get => bombLevel;
        protected set => bombLevel = Mathf.Clamp(value, 1, maxBombAllow);
    }

    [SerializeField, Range(1, 15)]
    private int blastLevel = 2;
    public int BlastLevel
    {
        get => blastLevel;
        protected set => blastLevel = Mathf.Clamp(value, 1, maxBlastAllow);
    }

    #endregion

    #region input region
    private void UpdatePlayerInput()
    {
        if (inputProfile == null)
            return;

        //check and pass movement input
        Vector2 inputMovement = Vector2.zero;
        if (Input.GetKey(inputProfile.Up))
            inputMovement = Vector2.up;
        else if (Input.GetKey(inputProfile.Down))
            inputMovement = Vector2.down;
        else if (Input.GetKey(inputProfile.Left))
            inputMovement = Vector2.left;
        else if (Input.GetKey(inputProfile.Right))
            inputMovement = Vector2.right;

        movement.Input = inputMovement;

        //check and trigger skill input
        if (Input.GetKeyDown(inputProfile.Bomb))
            PlantBomb();

        if (Input.GetKeyDown(inputProfile.Skill))
            ExecuteSkill();
    }
    #endregion

    #region BombControl
    private List<Bomb> activeBombs = new List<Bomb>();
    /// <summary>
    /// Number of active bomb
    /// </summary>
    public int ActiveBombCount => activeBombs.Count;
    #endregion

    protected virtual void Update()
    {
        UpdatePlayerInput();
    }

    private void PlantBomb()
    {
        if (ActiveBombCount >= BombLevel)
            return;
        
        var newbomb = Instantiate(normalBombPrefabs, playground.transform);
        newbomb.Setup(this, GridPosition);
        activeBombs.Add(newbomb);
        newbomb.onExploded += Newbomb_onExploded;
    }

    private void Newbomb_onExploded(Bomb explodedBomb)
    {
        if (activeBombs.Contains(explodedBomb))
            activeBombs.Remove(explodedBomb);
    }

    private void ExecuteSkill()
    {

    }
}