﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ParticleFX : FX
{
    [SerializeField]
    private ParticleSystem particleRef = null;

    public override void StartEffect()
    {
        base.StartEffect();
        particleRef.Play();
    }

//#if UNITY_EDITOR
//    [CustomEditor(typeof(ParticleFX))]
//    private class FXEditor : REditor.UnityObjectEditor<ParticleFX>
//    {
//        protected override void DrawingInspector()
//        {
//            if (handler.isActiveAndEnabled == true)
//            {
//                if (GUILayout.Button("Play Particle"))
//                {
//                    handler.particleRef.Play(true);
//                    //if (handler.particleRef.isPlaying)
//                    //    handler.particleRef.Stop();
//                    //else
//                    //    handler.particleRef.Play();
//                }
//            }
//        }
//    }
//#endif
}