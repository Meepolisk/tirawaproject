﻿using RTool.EzCanvas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LauncherUIController : MonoBehaviour
{
    [SerializeField]
    private List<BaseCanvas> initBaseCanvas = null;
    [SerializeField]
    private BaseCanvas bcanvasStart = null;
    
    public string SelectedSceneName { private set; get; }

    // Start is called before the first frame update
    void Start()
    {
        initBaseCanvas.ForEach(canvas =>
        {
            canvas.Show();
        });
    }

    public void SelectScene(string sceneName)
    {
        SelectedSceneName = sceneName;
        bcanvasStart.Show();
    }

    public void StartGame()
    {
        SceneManager.LoadScene(SelectedSceneName);
    }
}
