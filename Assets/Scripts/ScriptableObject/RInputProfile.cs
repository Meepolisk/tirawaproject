﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = nameof(RInputProfile), menuName = "ScriptableObjects/" + nameof(RInputProfile), order = 1)]
public class RInputProfile : ScriptableObject
{
    [SerializeField]
    private KeyCode up = KeyCode.W;
    public KeyCode Up => up;

    [SerializeField]
    private KeyCode down = KeyCode.S;
    public KeyCode Down => down;

    [SerializeField]
    private KeyCode left = KeyCode.A;
    public KeyCode Left => left;

    [SerializeField]
    private KeyCode right = KeyCode.D;
    public KeyCode Right => right;

    [SerializeField]
    private KeyCode bomb = KeyCode.J;
    public KeyCode Bomb => bomb;

    [SerializeField]
    private KeyCode skill = KeyCode.K;
    public KeyCode Skill => skill;
}