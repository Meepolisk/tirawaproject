﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTool.Utils;

public class Block : Doodad 
{
    [Header("Component ref")]
    [SerializeField]
    private Animator animator = null;

    [Header("Component ref")]
    [SerializeField,Range(0f,1f)]
    private float decayTime = 0.5f;
    
    public override void GotBombed(Bomb bomb)
    {
        animator.SetTrigger("Death");
        GCoroutine.DelayCall(decayTime, RemoveFromGame);

        //30% change drop item
        if (Random.Range(0f, 100f) < 30f)
            SpawnItem();
    }

    private void RemoveFromGame()
    {
        Destroy(gameObject);
    }

    private void SpawnItem()
    {

    }
}
