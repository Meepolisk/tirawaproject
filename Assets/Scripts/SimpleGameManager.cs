﻿using RTool.EzCanvas;
using RTool.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleGameManager : MonoBehaviour
{
    [Header("Component Ref")]
    [SerializeField]
    private PopupCanvas popupCanvas = null;

    private void OnEnable()
    {
        popupCanvas.onExit(() => UnityEngine.SceneManagement.SceneManager.LoadScene(0));
        Unit.aUnitDie += Unit_aUnitDie;
    }

    private void OnDisable()
    {
        Unit.aUnitDie -= Unit_aUnitDie;
    }

    private void Unit_aUnitDie(Unit dyingUnit, Unit killingUnit)
        => GCoroutine.DelayCall(2f, () => popupCanvas.Show());

    
}
