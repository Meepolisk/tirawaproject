﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTool;

public abstract class BObject : MonoBehaviour 
{
    [SerializeField]
    protected Playground playground;
    public Playground Playground => playground;

    public abstract Vector2Int GridPosition { get; }
}
