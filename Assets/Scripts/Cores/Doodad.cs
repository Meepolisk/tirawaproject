﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTool;

public abstract class Doodad : GridObject
{
    [SerializeField, Range(0.5f,2f)]
    private float awakeScale = 1.85f;

    public void EditorSetup(Playground playground, int x, int y, string name)
    {
        this.playground = playground;
        iPosition = new Vector2Int(x, y);
        this.name = string.Format("{0} {1}:{2}", name, x, y);
        gameObject.hideFlags = HideFlags.HideInHierarchy;

        RefreshPosition();
        RefreshScale();
    }
    public void RefreshScale()
    {
        transform.localScale = Vector3.one * awakeScale;
    }
}
