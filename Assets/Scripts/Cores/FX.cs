﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTool;

public abstract class FX : MonoBehaviour
{
    [SerializeField]
    private bool playOnAwake = true;
    [SerializeField, Range(0, 5f), Tooltip("Time in seconds that the FX will auto-destroy after spawned. Leave zero for manual destroy object")]
    private float timedLife = 0f;

    public virtual void StartEffect()
    {
        if (timedLife > 0)
            Destroy(gameObject, timedLife);
    }

    public FX Create(Vector3 position)
    {
        var fx = Instantiate(this);
        fx.transform.position = position;
        return fx;
    }

    protected virtual void Start()
    {
        if (playOnAwake)
            StartEffect();
    }
        
    public static FX Create(FX daFX, Vector3 position)
        => daFX.Create(position);
}
