﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTool;

public abstract class Item : GridObject
{
    public override void GotBombed(Bomb bomb)
    {
        Destroy(gameObject);
    }
}
