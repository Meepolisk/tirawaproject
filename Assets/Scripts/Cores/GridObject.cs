﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTool;

public abstract class GridObject : BObject 
{
    [SerializeField, ReadOnly]
    protected Vector2Int iPosition;
    public override Vector2Int GridPosition => iPosition;

    public abstract void GotBombed(Bomb bomb);

    protected virtual void Start()
    {
        Playground?.RegistObject(this);
    }
    protected virtual void OnDestroy()
    {
        Playground?.UnregistObject(this);
    }
    /// <summary>
    /// Reset the world position of this GridObject to its relative position of playground
    /// </summary>
    public virtual void RefreshPosition()
        => transform.localPosition = iPosition.ToLocalPosition(playground);
    
#if UNITY_EDITOR
    [UnityEditor.CustomEditor(typeof(GridObject), true, isFallback = true)]
    private class CustomEditor : REditor.UnityObjectEditor<GridObject>
    {
        protected override void DrawingInspector()
        {
            if (Application.isPlaying == false && handler.playground == null)
                UnityEditor.EditorGUILayout.HelpBox(string.Format("Warning: Missing {0}", nameof(Playground)), UnityEditor.MessageType.Warning);
        }
    }
#endif
}
