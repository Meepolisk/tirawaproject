﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTool;
using System;

public abstract class Bomb : GridObject 
{
    [SerializeField]
    private new Collider collider = null;

    [SerializeField]
    private FX blastEffect = null;
    [SerializeField]
    private FX fireEffect = null;

    /// <summary>
    /// Check if the bomb has exploded, or still alive
    /// </summary>
    public bool IsExploded { private set; get; }

    /// <summary>
    /// The hero that create this bomb
    /// </summary>
    public Hero Owner { protected set; get; }
    
    /// <summary>
    /// Event that raise whenever this bomb exploded
    /// </summary>
    public event Action<Bomb> onExploded;

    /// <summary>
    /// Should call this function when create bomb via code
    /// </summary>
    public virtual void Setup(Hero Owner, Vector2Int iPosition)
    {
        this.Owner = Owner;
        this.iPosition = iPosition;
        playground = Owner.Playground;
        //playground.RegistObject(this);

        RefreshPosition();
    }

    #region IgnoreUnitCollider
    // This region will allow units in the bomb gridcell at first can go throught the bomb

    private List<Unit> firstUnit = new List<Unit>();
    private void IgnoreUnitCollider()
    {
        var ListUnit = new List<Unit>(playground.GetUnitsInCell(GridPosition));
        if (ListUnit.Count > 0)
        {
            ListUnit.ForEach(unit =>
            {
                firstUnit.Add(unit);
                Physics.IgnoreCollision(collider, unit.Collider);
            });
            StartCoroutine(_CheckIgnoreUnitCollider());
        }
    }
    IEnumerator _CheckIgnoreUnitCollider()
    {
        while (firstUnit.Count > 0)
        {
            var toRemoves = firstUnit.FindAll(unit => unit.GridPosition != GridPosition);
            toRemoves.ForEach( toRemove =>
            {
                Physics.IgnoreCollision(collider, toRemove.Collider, false);
                firstUnit.Remove(toRemove);
            });
            yield return null;
        }
    }
    #endregion

    /// <summary>
    /// Invoke this one to make the bomb explode, immediately!!!
    /// </summary>
    public void Explode()
    {
        if (IsExploded == true)
            return;

        IsExploded = true;
        OnExplode();
        onExploded?.Invoke(this);
        Destroy(gameObject);
    }

    protected virtual void OnExplode()
    {
        blastEffect.Create(transform.position);

        //calculate stuff
        Vector2Int vec = GridPosition;
        BlastOnValidGrid(GridPosition);
        //right
        for (int i = 1; i <= Owner.BlastLevel; i++)
        {
            if (BlastOnValidGrid(new Vector2Int(vec.x + i, vec.y)))
                break;
        }
        //left
        for (int i = -1; i >= -Owner.BlastLevel; i--)
        {
            if (BlastOnValidGrid(new Vector2Int(vec.x + i, vec.y)))
                break;
        }
        //up
        for (int i = 1; i <= Owner.BlastLevel; i++)
        {
            if (BlastOnValidGrid(new Vector2Int(vec.x, vec.y + i)))
                break;
        }
        //down
        for (int i = -1; i >= -Owner.BlastLevel; i--)
        {
            if (BlastOnValidGrid(new Vector2Int(vec.x, vec.y + i)))
                break;
        }
    }
    private bool BlastOnValidGrid(Vector2Int vec)
    {
        if (vec.IsValidPosition(playground) == false)
            return false;

        fireEffect.Create(vec.ToLocalPosition(playground));
        foreach (var unit in playground.GetUnitsInCell(vec))
            unit.Dies(Owner);

        GridObject gridObject = vec.GetGridObject(playground);
        if (gridObject == null && gridObject != this)
            return false;
        gridObject.GotBombed(this);
        return true;
    }

    protected override void Start()
    {
        base.Start();
        IgnoreUnitCollider();
    }

    public override void GotBombed(Bomb bomb)
    {
        Explode();
    }

#if UNITY_EDITOR
    [UnityEditor.CustomEditor(typeof(Bomb))]
    private class CustomEditor : REditor.UnityObjectEditor<Bomb>
    {
        protected override void DrawingInspector()
        {
            if (Application.isPlaying == true)
            {
                if (GUILayout.Button(nameof(Bomb.Explode)))
                {
                    handler.Explode();
                }
            }
        }
    }
#endif
}

