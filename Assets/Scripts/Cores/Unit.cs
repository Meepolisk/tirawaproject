﻿using UnityEngine;

public abstract class Unit : BObject 
{
    [Header("Component Ref")]
    [SerializeField]
    protected new Collider collider = null;
    public Collider Collider => collider;

    [SerializeField]
    protected Animator animator = null;
    public Animator Animator => animator;

    [SerializeField]
    protected Movement movement = null;
    
    protected bool isAlive = true;
    public bool IsAlive => isAlive;

    public delegate void UnitDieDelegate (Unit dyingUnit, Unit killingUnit);
    public static event UnitDieDelegate aUnitDie;

    public override Vector2Int GridPosition
        => transform.position.ToGridPosition(playground);

    protected virtual void Start()
        => playground.RegistUnit(this);

    protected virtual void OnDestroy()
        => playground.UnregistUnit(this);

    public virtual void Dies(Unit killer)
    {
        isAlive = false;
        movement.IsEnabled = false;
        animator.SetBool("Run Forward", false);
        animator.SetTrigger("Die");
        aUnitDie?.Invoke(this, killer);
    }

    protected virtual void LateUpdate()
    {
        if (IsAlive)
            animator.SetBool("Run Forward", movement?.CurrentMovementVelocity > 0f);
    }
}
