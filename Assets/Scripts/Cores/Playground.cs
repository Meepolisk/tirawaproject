﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Playground : MonoBehaviour 
{
    [Header("Config")]
    [SerializeField, Range(2, 10)]
    private uint sizeY = 7;
    public uint SizeY => sizeY;
    public uint BlockHeight => sizeY * 2 + 1;

    [SerializeField, Range(2, 10)]
    private uint sizeX = 5;
    public uint SizeX => sizeX;
    public uint BlockWidth => sizeX * 2 + 1;
    
    [SerializeField, HideInInspector]
    private List<Block> blocks = new List<Block>();

    #region GridObject Controller
    private Dictionary<Vector2Int, GridObject> objectDicts = new Dictionary<Vector2Int, GridObject>();

    /// <summary>
    /// Regist GridObject into playground. Shouldn't care about this
    /// </summary>
    public bool RegistObject(GridObject gridObject)
    {
        Vector2Int vec = gridObject.GridPosition;
        if (vec.IsValidPosition(this) == false)
        {
            Debug.LogErrorFormat(gridObject, "Oh no, gridObject is not valid for this playground");
            return false;
        }
        if (objectDicts.ContainsKey(vec))
        {
            Object datObject = GetGridObject(vec);
            Debug.LogErrorFormat(datObject, "Logic bugged, already contain {0} at position {1}", datObject.name, vec.ToString());
            return false;
        }
        objectDicts.Add(vec, gridObject);
        return true;
    }
    /// <summary>
    /// Unregist GridObject from playground. Shouldn't care about this
    /// </summary>
    public bool UnregistObject(GridObject gridObject)
    {
        if (objectDicts.ContainsKey(gridObject.GridPosition) == false)
            return false;

        var datObject = objectDicts[gridObject.GridPosition];
        if (datObject != gridObject)
        {
            Debug.LogErrorFormat(datObject, "Logic bugged, the object at {0} is not the same with unregisting one", gridObject.GridPosition.ToString());
            return false;
        }
        objectDicts.Remove(gridObject.GridPosition);
        return true;
    }
    /// <summary>
    /// Get GridObject in this playground at position
    /// </summary>
    public GridObject GetGridObject(Vector2Int vec)
    {
        if (objectDicts.ContainsKey(vec) == false)
            return null;
        return objectDicts[vec];
    }
    #endregion

    #region Unit Controller
    private HashSet<Unit> unitList = new HashSet<Unit>();
    /// <summary>
    /// Number of unit in this playground
    /// </summary>
    public int ActiveUnitCount => unitList.Count;

    public bool RegistUnit(Unit unit)
    {
        if (unitList.Contains(unit) == true)
            return false;

        unitList.Add(unit);
        return true;
    }
    public bool UnregistUnit(Unit unit)
    {
        if (unitList.Contains(unit) == false)
            return false;

        unitList.Remove(unit);
        return true;
    }
    public IEnumerable<Unit> GetUnitsInCell(Vector2Int vec)
    {
        foreach(var unit in unitList)
        {
            if (unit.GridPosition.x == vec.x && unit.GridPosition.y == vec.y)
                yield return unit;
        }
    }
    #endregion

}

public static class PlaygroundExtensionMethods
{
    /// <summary>
    /// Check Vector2Int position on Playground if it is a static block
    /// </summary>
    public static bool IsStaticBlockPosition (this Vector2Int vec, Playground playground)
        => vec.x % 2 != 0 && vec.y % 2 != 0;

    /// <summary>
    /// Check Vector2Int position on Playground if it is the spawn location of players
    /// </summary>
    public static bool IsSpawnPosition (this Vector2Int vec, Playground playground)
        => (vec.x <= 1 || vec.x >= playground.BlockWidth - 2) && (vec.y <= 1 || vec.y >= playground.BlockHeight - 2);

    /// <summary>
    /// Check Vector2Int position on Playground if it is valid (not out of boundary)
    /// </summary>
    public static bool IsValidPosition(this Vector2Int vec, Playground playground)
        => vec.x >= 0 && vec.x < playground.BlockWidth && vec.y >= 0 && vec.y < playground.BlockHeight;

    /// <summary>
    /// Check Vector2Int position on Playground and return that GridObject
    /// </summary>
    public static GridObject GetGridObject(this Vector2Int vec, Playground playground)
        => playground.GetGridObject(vec);

    /// <summary>
    /// Check Vector2Int position on Playground and return that GridObject
    /// </summary>
    public static Vector3 ToLocalPosition(this Vector2Int vec, Playground playground)
        => new Vector3((vec.x * 2f) - (playground.BlockWidth - 1f), 1f, (vec.y * 2f) - (playground.BlockHeight - 1f));

    public static Vector2Int ToGridPosition(this Vector3 worldVec, Playground playground )
    {
        Vector3 localVec = worldVec;
        return new Vector2Int(Mathf.RoundToInt((localVec.x + (playground.BlockWidth - 1f)) / 2f),
                Mathf.RoundToInt((localVec.z + (playground.BlockHeight - 1f)) / 2f));
    }
}
