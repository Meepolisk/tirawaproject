﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : SingletonBase<InputController>
{
    [SerializeField]
    private RInputProfile player1 = null;
    public RInputProfile Player1 => player1;

    [SerializeField]
    private RInputProfile player2 = null;
    public RInputProfile Player2 => player2;

    [SerializeField]
    private RInputProfile player3 = null;
    public RInputProfile Player3 => player3;

    [SerializeField]
    private RInputProfile player4 = null;
    public RInputProfile Player4 => player4;

    private void Update()
    {
        
    }
    
}
