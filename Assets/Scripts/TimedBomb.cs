﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTool.Utils;

public class TimedBomb : Bomb 
{
    [SerializeField, Range(1f, 5f)]
    private float timer = 3f;

    protected override void Start()
    {
        base.Start();
        GCoroutine.DelayCall(timer, Explode);
    }
}

